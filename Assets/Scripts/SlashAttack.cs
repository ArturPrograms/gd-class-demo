﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashAttack : MonoBehaviour
{
    [SerializeField] int damage = 13;

    private SpriteRenderer m_sr;
    private Animator m_anim;
    private List<Collider2D> m_enteredAttack = new List<Collider2D>();

    private bool m_hasSentHit = false;
    private bool m_sendHit = false;
    private bool m_waitFrame = true;

    private void Start() {
        m_sr = GetComponent<SpriteRenderer>();
        m_anim = GetComponent<Animator>();
    }

    private void FixedUpdate() {
        if (!m_sendHit)
            return;

        if (m_waitFrame == false) {
            for (int i = 0; i < m_enteredAttack.Count; i++)
                m_enteredAttack[i].GetComponent<ZombieFSM>().takeDamage(damage);

            m_sendHit = false;
            m_hasSentHit = true;
            m_waitFrame = true;
        }
        else if(m_waitFrame == true) {
            m_waitFrame = false;
        }
    }

    public void startSlash() {
        m_anim.SetBool("isAttacking", true);
        m_sr.enabled = true;

        float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
        float mouseY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;

        float diffX = mouseX - transform.parent.position.x;
        float diffY = mouseY - transform.parent.position.y;

        Vector2 pos = new Vector2(diffX, diffY);
        pos.Normalize();
        

        transform.right = new Vector3(transform.position.x+pos.x, transform.position.y+pos.y, 0) - transform.position;
        m_sendHit = true;

        Invoke("endSlash", 0.25f);
    }

    void endSlash() {
        if (m_hasSentHit != true) {
            Invoke("endSlash", 0.25f);
            return;
        }

        m_sr.enabled = false;
        m_anim.SetBool("isAttacking", false);
        m_hasSentHit = false;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if(collision.CompareTag("Enemy")) {
            m_enteredAttack.Add(collision);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)  {
        if (collision.CompareTag("Enemy")) {
            m_enteredAttack.Remove(collision);
        }
    }
}

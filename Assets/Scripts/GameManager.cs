﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public void startGame() {
        SceneManager.LoadScene(1);
    }

    public void returnToMainMenu() {
        SceneManager.LoadScene(0);
    }

    public static void onPlayerDeath() {
        SceneManager.LoadScene(2);
    }
}

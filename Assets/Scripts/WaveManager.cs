using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public GameObject zombiePrefab;
    public PlayerFSM player;

    [SerializeField] float zombieRateConstant = 0.92f;
    [SerializeField] float zombieInitialRate = 2.0f;

    [SerializeField] Transform[] xBounds = new Transform[2];
    [SerializeField] Transform[] yBounds = new Transform[2];

    private int m_previousDeadZombies = 0;
    private int m_zombiesThisWave = 0;
    private float m_zombieSpawnRate = 0;

    private int m_currentWave = 0;
    private int m_zombiesSpawned = 0;
    private float m_waveTimer = 1.0f;

    private bool m_waveStarted = false;
    private bool m_wavePrepFinished = true;

    static int m_deadZombies = 0;

    void Start() {
        m_deadZombies = 0;
        m_zombieSpawnRate = zombieInitialRate;
    }

    void Update() {
        if (!player.isAlive())
            return;

        if (m_deadZombies - m_previousDeadZombies == m_zombiesThisWave && m_wavePrepFinished)
            m_waveStarted = false;

        if (!m_waveStarted) {
            Invoke("startNextWave", m_waveTimer);
            m_waveStarted = true;
            m_wavePrepFinished = false;
        }
    }

    void startNextWave() {
        // Prep Variables
        m_currentWave++;
        m_previousDeadZombies = m_deadZombies;

        // Wave Difficulty
        m_zombieSpawnRate *= zombieRateConstant;
        m_zombiesThisWave = (int)(m_currentWave * 3);
        m_zombiesSpawned = 0;

        m_wavePrepFinished = true;
        StartCoroutine(zombieSpawner());
    }

    IEnumerator zombieSpawner() {
        if (m_zombiesSpawned == m_zombiesThisWave)
            yield break;

        float randX = Random.Range(xBounds[0].position.x, xBounds[1].position.x);
        float randY = Random.Range(yBounds[0].position.y, yBounds[1].position.y);
        Instantiate(zombiePrefab, new Vector3(randX, randY, zombiePrefab.transform.position.z), zombiePrefab.transform.rotation);

        m_zombiesSpawned++;
        yield return new WaitForSeconds(m_zombieSpawnRate);
        StartCoroutine(zombieSpawner());
    }

    public static void addDeadZombie() { m_deadZombies++; }
}

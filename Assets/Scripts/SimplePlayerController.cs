using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

public class SimplePlayerController : MonoBehaviour
{
    public float moveSpeed = 3.0f;
    public int health = 0;

    public GameObject slashObject;
    public float slashRadius = 3.0f;

    [SerializeField] Sprite[] headSprites = new Sprite[4];
    [SerializeField] Sprite[] bodySprites = new Sprite[4];

    [SerializeField] Color hitFlashColor = new Color(1, 0, 0);
    [SerializeField] float flashTimer = 0.3f;
    [SerializeField] float flashCount = 2;

    [SerializeField] Text healthText;
    [SerializeField] Image healthBar;

    private Rigidbody2D m_rb2D;
    private Animator m_anim;

    private int m_maxHealth;
    private bool m_isAlive = true;

    Stopwatch m_sw = new Stopwatch();
    bool m_attacked = false;
    bool m_hasAttacked = false;
    float m_attackRate = 0.75f;

    private void Start()
    {
        m_rb2D = GetComponent<Rigidbody2D>();
        m_anim = GetComponent<Animator>();

        m_maxHealth = health;
        healthText.text = health.ToString();
    }

    private void Update()
    {
        // Handle States
        if (Input.GetMouseButtonDown(0) && !m_attacked) {
            m_attacked = true;
            m_hasAttacked = false;
            m_sw.Start();
        }

        if (m_attacked == true) {
            if (m_sw.ElapsedMilliseconds / 1000 >= m_attackRate) {
                m_attacked = false;
                m_sw.Stop();
                m_sw.Reset();
            }

            if (!m_hasAttacked) {
                float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
                float mouseY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;

                float diffX = mouseX - transform.position.x;
                float diffY = mouseY - transform.position.y;

                Vector2 pos = new Vector2(diffX, diffY);
                pos.Normalize();
                pos *= slashRadius;
                pos.x += transform.position.x;
                pos.y += transform.position.y;

                slashObject.transform.position = new Vector3(pos.x, pos.y, 0);
                slashObject.GetComponent<SlashAttack>().startSlash();

                m_hasAttacked = true;
            }
        }

        float horizontalMovement = Input.GetAxis("Horizontal") * moveSpeed;
        float verticalMovement = Input.GetAxis("Vertical") * moveSpeed;

        m_rb2D.AddForce(new Vector2(horizontalMovement * Time.deltaTime, verticalMovement * Time.deltaTime), ForceMode2D.Impulse);

        // Set Sprite Based On Mouse Pos
        this.updateSprite();
    }

    private void updateSprite()
    {
        // Change sprite based on the angle the player is moving
        float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;

        float mouseY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;

        float angle = Mathf.Atan2(mouseY - transform.position.y, mouseX - transform.position.x);
        float myPI = 3.141593f;

        if ((((myPI / 2.0f) / 2.0f) >= angle && 0.0f <= angle) || (-((myPI / 2.0f) / 2.0f) <= angle && 0.0f >= angle))
        {
            // Looking Right
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[3];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[3];
        }
        else if ((-((myPI / 2.0f) + ((myPI / 2.0f) / 2.0f)) >= angle && -myPI <= angle) || (myPI >= angle && ((myPI / 2.0f) + ((myPI / 2.0f) / 2.0f)) <= angle))
        {
            // Looking Left
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[2];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[2];
        }
        else if ((((myPI / 2.0f) / 2.0f) < angle && (myPI / 2.0f) >= angle) || ((myPI / 2.0f) <= angle && ((myPI / 2.0f) + ((myPI / 2.0f) / 2.0f)) > angle))
        {
            // Looking Up
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[1];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[1];
        }
        else
        {
            // Looking Down
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[0];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[0];
        }
    }

    IEnumerator onHitFlash()
    {
        for (int i = 0; i < flashCount; i++)
        {
            transform.GetChild(1).GetComponent<SpriteRenderer>().color = hitFlashColor;
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = hitFlashColor;
            yield return new WaitForSeconds(flashTimer);
            transform.GetChild(1).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            yield return new WaitForSeconds(flashTimer);
        }

        yield break;
    }

    public bool isAlive() { return m_isAlive; }

    public void takeDamage(int damage)
    {
        health -= damage;

        // Check if player died
        if (health <= 0)
        {
            health = 0;
            GameManager.onPlayerDeath();
        }
        else
        {
            StartCoroutine(onHitFlash());
        }

        healthText.text = health.ToString();
        healthBar.fillAmount = (float)health / (float)m_maxHealth;
    }
}

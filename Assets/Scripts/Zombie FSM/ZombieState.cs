﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ZombieState
{
    void enterState(ZombieFSM zFSM, string previousState);
    void exitState(ZombieFSM zFSM, string nextState);
    ZombieState updateState(ZombieFSM zFSM);
    string toString();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieIdleState : ZombieState
{
    public void enterState(ZombieFSM zFSM, string previousState) {
        // Enter Animation
        zFSM.setAnimBool("isChasing", false);
        zFSM.setAnimBool("isAttacking", false);
    }

    public void exitState(ZombieFSM zFSM, string nextState) {
        // Do Nothing
    }

    public ZombieState updateState(ZombieFSM zFSM) {
        if (zFSM.damageToBeTaken > 0)
            return new ZombieTakeHitState();

        float diffX = zFSM.transform.position.x - zFSM.playerPos.position.x;
        float diffY = zFSM.transform.position.y - zFSM.playerPos.position.y;

        if (Mathf.Sqrt((diffX * diffX) + (diffY * diffY)) <= zFSM.chaseRange)
            return new ZombieChaseState();

        return null;
    }

    public string toString() {
        return "Zombie Idle";
    }
}

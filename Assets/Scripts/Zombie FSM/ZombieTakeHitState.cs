﻿using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;
using UnityEngine;

public class ZombieTakeHitState : ZombieState
{
    Stopwatch m_sw = new Stopwatch();
    bool m_tookHit = false;

    public void enterState(ZombieFSM zFSM, string previousState) {
        // Enter Animation (No Animation Yet)

        // Initialize Resources
        m_sw.Start();
    }

    public void exitState(ZombieFSM zFSM, string nextState) {
        // Exit Animation (No Animation Yet)

        // Clean up resources
        m_sw.Stop();
        m_tookHit = false;
    }

    public ZombieState updateState(ZombieFSM zFSM) {
        if (m_tookHit == false) {
            float diffX = zFSM.playerPos.position.x - zFSM.transform.position.x;
            float diffY = zFSM.playerPos.position.y - zFSM.transform.position.y;
            Vector2 vecToPlayer = new Vector2(diffX, diffY);

            // Chase
            zFSM.physicsImpulse(-vecToPlayer.normalized * zFSM.knockbackDistance);

            m_tookHit = true;
        }

        if (m_sw.ElapsedMilliseconds / 1000 >= zFSM.knockbackTime) {
            zFSM.damageToBeTaken = 0;
            return new ZombieIdleState();
        }

        return null;
    }

    public string toString() {
        return "Zombie Take Hit";
    }
}

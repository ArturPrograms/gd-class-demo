﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class ZombieAttackState : ZombieState
{
    PlayerFSM m_pFSM;

    Stopwatch m_sw = new Stopwatch();
    bool m_canAttack = true;

    public void enterState(ZombieFSM zFSM, string previousState) {
        // Enter Animation
        zFSM.setAnimBool("isAttacking", true);

        // Initialize Resources
        m_pFSM = zFSM.playerPos.gameObject.GetComponent<PlayerFSM>();
        m_sw.Start();
    }

    public void exitState(ZombieFSM zFSM, string nextState) {
        // Exit Animation
        zFSM.setAnimBool("isAttacking", false);

        // Clean Up Resources
        m_sw.Stop();
    }

    public ZombieState updateState(ZombieFSM zFSM) {
        // Attack
        if (m_canAttack) {
            m_pFSM.takeDamage(zFSM.attackDamage);
            m_canAttack = false;
        }

        // Slow down to attack rate
        if (m_sw.ElapsedMilliseconds / 1000 >= zFSM.attackRate) {
            m_sw.Restart();
            m_canAttack = true;
        }

        // Make sure we are in range
        float diffX = zFSM.playerPos.position.x - zFSM.transform.position.x;
        float diffY = zFSM.playerPos.position.y - zFSM.transform.position.y;

        if (zFSM.damageToBeTaken > 0)
            return new ZombieTakeHitState();

        if (Mathf.Sqrt((diffX * diffX) + (diffY * diffY)) >= zFSM.attackRange)
            return new ZombieChaseState();

        return null; // Keep attacking if we are still in range
    }

    public string toString() {
        return "Zombie Attack";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieChaseState : ZombieState
{
    public void enterState(ZombieFSM zFSM, string previousState) {
        // Enter Animation
        zFSM.setAnimBool("isChasing", true);
    }

    public void exitState(ZombieFSM zFSM, string nextState) {
        // Exit Animation
        zFSM.setAnimBool("isChasing", false);
    }

    public ZombieState updateState(ZombieFSM zFSM) {
        if (zFSM.damageToBeTaken > 0)
            return new ZombieTakeHitState();

        float diffX = zFSM.playerPos.position.x - zFSM.transform.position.x;
        float diffY = zFSM.playerPos.position.y - zFSM.transform.position.y;
        float distToPlayer = Mathf.Sqrt((diffX * diffX) + (diffY * diffY));
        Vector2 vecToPlayer = new Vector2(diffX, diffY);

        if (distToPlayer >= zFSM.chaseRange)
            return new ZombieIdleState();

        if (distToPlayer <= zFSM.attackRange)
            return new ZombieAttackState();

        // Chase
        zFSM.physicsImpulse(vecToPlayer.normalized * zFSM.speed * Time.deltaTime);

        return null;
    }

    public string toString() {
        return "Zombie Chase";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ZombieFSM : MonoBehaviour
{
    public int health;
    public int attackDamage;
    public float speed;
    public float chaseRange;
    public float knockbackDistance;
    public float knockbackTime;
    public float attackRange;
    public float attackRate;

    [SerializeField] float flashTimer = 0.3f;
    [SerializeField] float flashCount = 2;
    private Shader m_guiTextShader;
    private Shader m_defaultShader;

    public Transform playerPos;

    [SerializeField] Sprite[] headSprites = new Sprite[4];
    [SerializeField] Sprite[] bodySprites = new Sprite[4];

    private ZombieState m_currentState = new ZombieIdleState();

    private Rigidbody2D m_rb2D;
    private Animator m_anim;

    // Public For State Machine
    public int damageToBeTaken = 0;

    private void Start() {
        m_rb2D = GetComponent<Rigidbody2D>();
        m_anim = GetComponent<Animator>();
        playerPos = GameObject.Find("Player").transform;

        m_guiTextShader = Shader.Find("GUI/Text Shader");
        m_defaultShader = Shader.Find("Sprites/Default");

        m_currentState.enterState(this, "");
    }

    private void Update() {
        ZombieState new_state = m_currentState.updateState(this);
        if(new_state != null) {
            m_currentState.exitState(this, new_state.toString());
            new_state.enterState(this, m_currentState.toString());

            m_currentState = new_state;
        }
    }

    IEnumerator onHitFlash() {
        for (int i = 0; i < flashCount; i++) {
            transform.GetChild(1).GetComponent<SpriteRenderer>().material.shader = m_guiTextShader;
            transform.GetChild(0).GetComponent<SpriteRenderer>().material.shader = m_guiTextShader;
            yield return new WaitForSeconds(flashTimer);
            transform.GetChild(1).GetComponent<SpriteRenderer>().material.shader = m_defaultShader;
            transform.GetChild(0).GetComponent<SpriteRenderer>().material.shader = m_defaultShader;
            yield return new WaitForSeconds(flashTimer);
        }

        yield break;
    }

    public void setAnimBool(string boolName, bool value) {
        m_anim.SetBool(boolName, value);
    }

    public void physicsImpulse(Vector2 move) {
        m_rb2D.AddForce(move, ForceMode2D.Impulse);
        
    }

    public void takeDamage(int dmg) {
        damageToBeTaken = dmg;
        health -= dmg;

        // Check if died
        if(health <= 0) {
            WaveManager.addDeadZombie();
            Destroy(gameObject);
        }

        StartCoroutine(onHitFlash());
    }
}

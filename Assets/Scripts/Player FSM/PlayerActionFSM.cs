﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionFSM : MonoBehaviour
{
    public GameObject slashObject;
    public float slashRadius = 3.0f;

    PlayerActionState m_currentState = new PlayerActIdle();

    private void Start() {
        m_currentState.enterState(this, "");
    }

    private void Update() {
        PlayerActionState new_state = m_currentState.updateState(this);
        if(new_state != null) {
            m_currentState.exitState(this, new_state.toString());
            new_state.enterState(this, m_currentState.toString());

            m_currentState = new_state;
        }
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, 1);
    }
}

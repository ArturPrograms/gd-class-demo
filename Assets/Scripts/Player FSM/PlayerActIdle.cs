﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActIdle : PlayerActionState
{
    public void enterState(PlayerActionFSM pFSM, string previousState) {
        // Do Nothing
    }

    public void exitState(PlayerActionFSM pFSM, string nextState) {
        // Do Nothing
    }

    public PlayerActionState updateState(PlayerActionFSM pFSM) {
        if (Input.GetMouseButtonDown(0))
            return new PlayerActSlash();

        return null;
    }

    public string toString() {
        return "Player Action Idle";
    }
}

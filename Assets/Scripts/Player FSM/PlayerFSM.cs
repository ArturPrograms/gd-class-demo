﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerFSM : MonoBehaviour
{
    public float moveSpeed = 3.0f;
    public int health = 0;

    [SerializeField] Sprite[] headSprites = new Sprite[4];
    [SerializeField] Sprite[] bodySprites = new Sprite[4];

    [SerializeField] Color hitFlashColor = new Color(1, 0, 0);
    [SerializeField] float flashTimer = 0.3f;
    [SerializeField] float flashCount = 2;


    [SerializeField] Text healthText;
    [SerializeField] Image healthBar;

    private PlayerState m_currentState = new PlayerIdleState();

    private Rigidbody2D m_rb2D;
    private Animator m_anim;

    private int m_maxHealth;
    private bool m_isAlive = true;

    private void Start() {
        m_rb2D = GetComponent<Rigidbody2D>();
        m_anim = GetComponent<Animator>();

        m_currentState.enterState(this, "");

        m_maxHealth = health;
        healthText.text = health.ToString();
    }

    private void Update() {
        PlayerState new_state = m_currentState.updateState(this);

        // Transition States
        if(new_state != null) {
            m_currentState.exitState(this, new_state.toString());
            new_state.enterState(this, m_currentState.toString());

            m_currentState = new_state;
        }

        // Set Sprite Based On Mouse Pos
        this.updateSprite();
    }

    private void updateSprite() {
        // Change sprite based on the angle the player is moving
        float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;

        float mouseY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;

        float angle = Mathf.Atan2(mouseY - transform.position.y, mouseX - transform.position.x);
        float myPI = 3.141593f;

        if ((((myPI / 2.0f) / 2.0f) >= angle && 0.0f <= angle) || (-((myPI / 2.0f) / 2.0f) <= angle && 0.0f >= angle)) {
            // Looking Right
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[3];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[3];
        }
        else if ((-((myPI / 2.0f) + ((myPI / 2.0f) / 2.0f)) >= angle && -myPI <= angle) || (myPI >= angle && ((myPI / 2.0f) + ((myPI / 2.0f) / 2.0f)) <= angle)) {
            // Looking Left
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[2];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[2];
        }
        else if ((((myPI / 2.0f) / 2.0f) < angle && (myPI / 2.0f) >= angle) || ((myPI / 2.0f) <= angle && ((myPI / 2.0f) + ((myPI / 2.0f) / 2.0f)) > angle)) {
            // Looking Up
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[1];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[1];
        }
        else {
            // Looking Down
            transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = headSprites[0];
            transform.GetChild(1).GetComponent<SpriteRenderer>().sprite = bodySprites[0];
        }
    }

    IEnumerator onHitFlash() {
        for(int i = 0; i < flashCount; i++) {
            transform.GetChild(1).GetComponent<SpriteRenderer>().color = hitFlashColor;
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = hitFlashColor;
            yield return new WaitForSeconds(flashTimer);
            transform.GetChild(1).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            transform.GetChild(0).GetComponent<SpriteRenderer>().color = new Color(1, 1, 1);
            yield return new WaitForSeconds(flashTimer);
        }

        yield break;
    }

    public bool isAlive() { return m_isAlive; }

    public void takeDamage(int damage) {
        health -= damage;

        // Check if player died
        if (health <= 0) {
            health = 0;
            GameManager.onPlayerDeath();
        } 
        else {
            StartCoroutine(onHitFlash());
        }

        healthText.text = health.ToString();
        healthBar.fillAmount = (float)health / (float)m_maxHealth;
    }

    public void setAnimBool(string boolName, bool value) {
        m_anim.SetBool(boolName, value);
    }

    public void physicsImpulse(Vector2 move) {
        m_rb2D.AddForce(move, ForceMode2D.Impulse);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : PlayerState
{
    public void enterState(PlayerFSM pFSM, string previousState) {
        pFSM.setAnimBool("isRunning", false);
    }

    public void exitState(PlayerFSM pFSM, string nextState) {
        // Do Nothing
    }

    public PlayerState updateState(PlayerFSM pFSM) {
        if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
            return new PlayerRunState();

        return null;
    }

    public string toString() {
        return "Player Idle";
    }
}

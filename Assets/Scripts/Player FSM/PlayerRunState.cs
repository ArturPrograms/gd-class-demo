﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRunState : PlayerState
{
    public void enterState(PlayerFSM pFSM, string previousState) {
        pFSM.setAnimBool("isRunning", true);
    }

    public void exitState(PlayerFSM pFSM, string nextState) {
        // Do Nothing
    }

    public PlayerState updateState(PlayerFSM pFSM) {
        float horizontalMovement = Input.GetAxis("Horizontal") * pFSM.moveSpeed;
        float verticalMovement = Input.GetAxis("Vertical") * pFSM.moveSpeed;

        pFSM.physicsImpulse(new Vector2(horizontalMovement*Time.deltaTime, verticalMovement*Time.deltaTime));

        if (horizontalMovement != 0 || verticalMovement != 0)
            return null;
        else
            return new PlayerIdleState();
    }

    public string toString() {
        return "Player Run";
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PlayerActionState
{
    void enterState(PlayerActionFSM pFSM, string previousState);
    void exitState(PlayerActionFSM pFSM, string nextState);
    PlayerActionState updateState(PlayerActionFSM pFSM);
    string toString();
}

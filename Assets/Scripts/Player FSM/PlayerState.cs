﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface PlayerState
{
    void enterState(PlayerFSM pFSM, string previousState);
    void exitState(PlayerFSM pFSM, string nextState);
    PlayerState updateState(PlayerFSM pFSM);
    string toString();
}

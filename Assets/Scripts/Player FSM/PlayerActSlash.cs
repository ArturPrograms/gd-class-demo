﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class PlayerActSlash : PlayerActionState
{
    Stopwatch m_sw  = new Stopwatch();
    bool m_attacked = false;
    float m_attackRate = 0.75f;

    public void enterState(PlayerActionFSM pFSM, string previousState) {
        m_sw.Start();
        m_attacked = false;
    }

    public void exitState(PlayerActionFSM pFSM, string nextState) {
        m_sw.Stop();
        m_attacked = false;
    }

    public PlayerActionState updateState(PlayerActionFSM pFSM) {
        if (m_sw.ElapsedMilliseconds / 1000 >= m_attackRate)
            return new PlayerActIdle();

        if (m_attacked)
            return null;

        float mouseX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x;
        float mouseY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y;

        float diffX = mouseX - pFSM.transform.position.x;
        float diffY = mouseY - pFSM.transform.position.y;

        Vector2 pos = new Vector2(diffX, diffY);
        pos.Normalize();
        pos *= pFSM.slashRadius;
        pos.x += pFSM.transform.position.x;
        pos.y += pFSM.transform.position.y;

        pFSM.slashObject.transform.position = new Vector3(pos.x, pos.y, 0);
        pFSM.slashObject.GetComponent<SlashAttack>().startSlash();

        m_attacked = true;

        return null;
    }

    public string toString() {
        return "Player Action Slash";
    }
}

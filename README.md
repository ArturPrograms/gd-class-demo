# README #

Steps necessary to run the code

### What is this repository for? ###

* Small simple game meant to showcase Unity's simple 2D game techniques.
* Easy to understand and replicate
* Fun to mess around and add your own things

### How do I get set up? ###

* Just clone the project and open it with Unity version 2020.1.8f
* Click on "Add" in the project section of the Unity Hub and select the cloned project folder

### Contribution guidelines ###

* Artur Sahakyan

### Who do I talk to? ###

* Person listed above :D